#!/bin/sh

CMD=$1

install() {
    # install sudo
    if [ -z $(which sudo) ]; then
        echo "Using apt-get install sudo"
        su -c "apt-get update && apt-get install sudo && sudo adduser $USER sudo && /sbin/reboot"
        exit 1
    fi

    if [ $(id -u) -ne 0 ]; then
        echo "Permission denied, please run as sudo."
        exit 1
    fi

    if [ -z $(which wget) ]; then
        su -c "apt-get wget"
    fi

    if [ ! -f "epc-prod.yml" ]; then
        wget -q https://gitlab.com/sigmawu/engenius_epc/-/raw/main/epc-prod.yml
        if [ $? -ne 0 ]; then
            echo "Download yml file failed."
            exit 1
        fi
    fi

    # Read account information
    printf "Please configure your default account.\n"
    email_pattern="^[a-zA-Z0-9\_\.\+\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$"
    while true
    do
        printf "Email: "
        read -r email
        if printf "$email" | egrep -q "$email_pattern"; then
            break
        else
            printf "Invalid email address\n"
            continue
        fi
    done

    while true
    do
        printf "Password: "
        read -r new_password
        text_pattern=".*[a-z].*"
        digit_pattern=".*[0-9].*"
        if ( [ ${#new_password} -ge 8 ] && printf "%s" "$new_password" | egrep -q "$text_pattern" && printf "%s" "$new_password" | egrep -q "$digit_pattern" ); then
            printf "Confirm Password: "
            read -r confirm_password
            if [ $new_password = $confirm_password ]; then
                break
            else
                printf "Sorry, passwords do not match.\n"
            fi
        else
            printf "At least 8 characters. Contains a minimum of 1 lowercase letter & digit.\n"
        fi
        continue
    done

    printf "First Name: " && read -r first_name
    printf "Last Name: " && read -r last_name

    # install docker engine
    if [ -z $(which docker) ]; then
        apt-get update
        apt-get install -y apt-transport-https ca-certificates curl gnupg lsb-release
        # get OS information and convert to lowercase
        OS=$(lsb_release -si | tr '[:upper:]' '[:lower:]')
        curl -fsSL https://download.docker.com/linux/$OS/gpg | sudo gpg --batch --yes --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
        echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/$OS $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
        apt-get update
        apt-get install -y docker-ce docker-ce-cli containerd.io
    fi

    # install docker compose
    if [ -z $(which docker-compose) ]; then
        sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
        sudo chmod +x /usr/local/bin/docker-compose
    fi

    # pull image or use local image
    if [ -z "$1" ]; then
        REPOSITORY_URI=public.ecr.aws/g8k4a9z9/
        echo "REPOSITORY_URI=${REPOSITORY_URI}" > .epc-prod
        echo "VERSION=latest" >> .epc-prod
        echo "HOST_IP=$(hostname -I | awk {'print $1'})" >> .epc-prod

        # pull images
        docker pull "${REPOSITORY_URI}epc-api:latest"
        docker pull "${REPOSITORY_URI}epc-db:latest"
        docker pull "${REPOSITORY_URI}epc-mdns:latest"
        docker pull "${REPOSITORY_URI}epc-otter:latest"
        docker pull "${REPOSITORY_URI}epc-raccoon:latest"
        docker pull "${REPOSITORY_URI}epc-radius:latest"
    elif [ "$1" = "ova" ]; then
        if [ -z "$2" ]; then
            echo "Usage ECR: epc-prod.sh install.(using the latest image from aws)"
            echo "Usage OVA: epc-prod.sh install ova \${version}.(using local image)"
        else
            echo "REPOSITORY_URI=" > .epc-prod
            echo "VERSION=$2" >> .epc-prod
            echo "HOST_IP=$(hostname -I | awk {'print $1'})" >> .epc-prod
        fi
    else
        echo "Usage ECR: epc-prod.sh install.(using the latest image from aws)"
        echo "Usage OVA: epc-prod.sh install ova \${version}.(using local image)"
    fi

    run
    init_db_from_container

    # Update account information
    encryption_password=`docker exec -it epc-api sh -c "cd /app/pkg/general && cat << EOF > /tmp/npd
$new_password
EOF
    python - <<EOF
import os
import utils
filepath = '/tmp/npd'
with open(filepath) as f:
    print(utils.password_aes_encryption(f.readlines()[0].rstrip()))
os.remove(filepath)
EOF"`

    encryption_password_without_carriage=$(echo $encryption_password | sed -e 's/\r//g')
    #docker exec -it epc-db sh -c '/db-init.sh update_account '$email' '$encryption_password_without_carriage' '$first_name' '$last_name' >/dev/null 2>&1'
    docker exec -it epc-api sh -c 'python /app/db-init.pyc -u '$email' '$encryption_password_without_carriage' '$first_name' '$last_name' >/dev/null 2>&1'
}

uninstall() {
    # remove container
    docker-compose -f epc-prod.yml --env-file .epc-prod down

    # remove images
    REPOSITORY_URI=$(sed '1!d' .epc-prod | cut -d'=' -f 2)
    TAG=$(sed '2!d' .epc-prod | cut -d'=' -f 2)
    docker rmi ${REPOSITORY_URI}epc-api:${TAG}
    docker rmi ${REPOSITORY_URI}epc-db:${TAG}
    docker rmi ${REPOSITORY_URI}epc-mdns:${TAG}
    docker rmi ${REPOSITORY_URI}epc-otter:${TAG}
    docker rmi ${REPOSITORY_URI}epc-raccoon:${TAG}
    docker rmi ${REPOSITORY_URI}epc-radius:${TAG}

    # remove .epc-prod file
    rm .epc-prod

    # delete volumes
    rm -rf /srv/docker/mongodb/data/db
    rm -rf /mount/image
    rm -rf /mount/firmware
}

remove() {
    # remove container
    docker-compose -f epc-prod.yml --env-file .epc-prod down

    # delete volumes
    rm -rf /srv/docker/mongodb/data/db
    rm -rf /mount/image
    rm -rf /mount/firmware
}

init_db_from_container() {
    #docker exec -it epc-db sh -c '/db-init.sh container >/dev/null 2>&1'
    docker exec -it epc-api sh -c 'python /app/db-init.pyc -i >/dev/null 2>&1'
}

run() {
    docker-compose -f epc-prod.yml --env-file .epc-prod up -d
    hostname -I | awk {'print "EPC_IP: " $1 ":8080"'}
}

stop() {
    docker-compose -f epc-prod.yml --env-file .epc-prod stop
}

case "$CMD" in
    install)
        install $2 $3
        RETVAL=1
        ;;
    uninstall)
        uninstall
        RETVAL=1
        ;;
    remove)
        remove
        RETVAL=1
        ;;
    run)
        run
        RETVAL=1
        ;;
    stop)
        stop
        RETVAL=1
        ;;
    *)
        echo "Usage: $0 [install, uninstall, remove, run, stop]"
        echo "Usage ECR: epc-prod.sh install.(using the latest image from aws)"
        echo "Usage OVA: epc-prod.sh install ova \${version}.(using local image)"
        RETVAL=1
esac
exit $RETVAL