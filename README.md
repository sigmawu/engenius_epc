# engenius_epc



## Platform Requirements
- PC/server with minimum specs:
    - Platform: X86-based PC, Server, or Cloud Instance
    - CPU: Intel/AMD, computing power >= i3, 2-core is preferred.
    - Memory: 1GB free space for running PrivateCloud
    - Storage: 8GB

## Environment Requirements
Currently Engenius Private Cloud has been tested on the following linux distributations:
- Ubuntu: [20.04.3 LTS(Server)](https://releases.ubuntu.com/20.04.3)
- Debian: [10.6](https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/archive/10.6.0+nonfree/amd64/iso-dvd/)

Besides, ```sudo``` is required before Engenius Private Cloud installation.

## Getting Started
- Download the install-script in Linux Shell:
```
wget https://gitlab.com/sigmawu/engenius_epc/-/raw/main/docker-compose-build.sh
wget https://gitlab.com/sigmawu/engenius_epc/-/raw/main/docker-compose-build.yml
```
- Make the downloaded file excutable:
```
chmod 755 docker-compose-build.sh
```

## Usage
- Make sure ```docker-compose-build.sh``` and ```docker-compose-build.yml``` are in the same directory.
- Install and run Engenius Private Cloud:
```
sudo ./docker-compose-build.sh install
```

- Stop the running Engenius Private Cloud containers:
```
sudo ./docker-compose-build.sh stop
```

- Run the stopped Engenius Private Cloud containers:
```
sudo ./docker-compose-build.sh run
```

- Remove Engenius Private Cloud containers but keep the images:
```
sudo ./docker-compose-build.sh remove
```

- Remove Engenius Private Cloud containers and images:
```
sudo ./docker-compose-build.sh uninstall
```
