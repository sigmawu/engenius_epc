import argparse
import logging
import socket
import netifaces
from time import sleep
from zeroconf import IPVersion, ServiceInfo, Zeroconf

ETH_INTERFACE_DEBIAN = "eth"
ETH_INTERFACE_UBUNTU = "enp0s"
MINICLOUD_MDNS_SERVICE_NAME = "Minicloud"
MINICLOUD_MDNS_SERVICE_PROTOCOL = "._http._tcp.local."
CHECKIN_PORT = 8080
RACCOON_PORT = 80

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)

    #Get eth0 ip
    ipv4 = "0.0.0.0"
    mac = "FF:FF:FF:FF:FF:FF"
    interfaces = netifaces.interfaces()
    for interface in interfaces:
        if ETH_INTERFACE_DEBIAN in interface or ETH_INTERFACE_UBUNTU in interface:
            address = netifaces.ifaddresses(interface)
            ipv4_info = address.get(netifaces.AF_INET)[0]
            ipv4 = ipv4_info.get("addr")
            mac = address.get(netifaces.AF_LINK)[0].get("addr").upper()
            break

    parser = argparse.ArgumentParser()
    parser.add_argument("--debug", action="store_true")
    version_group = parser.add_mutually_exclusive_group()
    version_group.add_argument("--v6", action="store_true")
    version_group.add_argument("--v6-only", action="store_true")
    args = parser.parse_args()

    if args.debug:
        logging.getLogger("zeroconf").setLevel(logging.DEBUG)
    ip_version = IPVersion.V4Only

    desc = {"checkin_port": CHECKIN_PORT,
            "checkin_scheme": "http",
            "checkin_path": "/api/v1/checkin",
            "time_path": "/time",
            "raccoon_port": RACCOON_PORT,
            "raccoon_scheme": "http",
            "raccoon_register_path": "/device/register"}

    #Use mac to set the mdns service name to make sure it is unique.
    mac_string = mac.replace(":", "")
    service_name_postfix = "_" + mac_string[6:]
 
    info = ServiceInfo(
        "_http._tcp.local.",
        MINICLOUD_MDNS_SERVICE_NAME + service_name_postfix + MINICLOUD_MDNS_SERVICE_PROTOCOL,
        addresses=[socket.inet_aton(ipv4)],
        port=80,
        properties=desc,
    )

    zeroconf = Zeroconf(ip_version=ip_version)
    zeroconf.register_service(info)
    
    try:
        while True:
            sleep(10)
    except KeyboardInterrupt:
        pass
    finally:
        print("Unregistering...")
        zeroconf.unregister_service(info)
        zeroconf.close()
